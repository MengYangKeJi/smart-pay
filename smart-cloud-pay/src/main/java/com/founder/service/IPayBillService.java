package com.founder.service;

import com.founder.core.domain.FianceAl;
import com.founder.core.domain.FianceWx;

import java.util.List;

public interface IPayBillService {

    /**
     * 删除微信对账单记录
     * @param mchId
     * @param billDate
     * @return
     */
    int deleteWx(String mchId, String billDate);

    /**
     * 删除支付宝对账单记录
     * @param mchId
     * @param billDate
     * @return
     */
    int deleteAli(String mchId, String billDate);

    /**
     * 查询对账单
     * @param mchId
     * @param billDate
     * @param keyName
     * @return
     */
    List<FianceWx> selectWx(String mchId, String billDate, String keyName);

    /**
     * 查询对账单
     * @param mchId
     * @param billDate
     * @param keyName
     * @return
     */
    List<FianceAl> selectAli(String mchId, String billDate, String keyName);

    /**
     * 保存对账单
     * @param list
     * @return
     */
    List<FianceWx> saveWx(List<FianceWx> list);

    /**
     * 保存对账单
     * @param list
     * @return
     */
    List<FianceAl> saveAli(List<FianceAl> list);
}
