package com.founder.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FianceWxExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    private Integer limit;

    private Integer offset;

    public FianceWxExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMchidIsNull() {
            addCriterion("mchid is null");
            return (Criteria) this;
        }

        public Criteria andMchidIsNotNull() {
            addCriterion("mchid is not null");
            return (Criteria) this;
        }

        public Criteria andMchidEqualTo(String value) {
            addCriterion("mchid =", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotEqualTo(String value) {
            addCriterion("mchid <>", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidGreaterThan(String value) {
            addCriterion("mchid >", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidGreaterThanOrEqualTo(String value) {
            addCriterion("mchid >=", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidLessThan(String value) {
            addCriterion("mchid <", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidLessThanOrEqualTo(String value) {
            addCriterion("mchid <=", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidLike(String value) {
            addCriterion("mchid like", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotLike(String value) {
            addCriterion("mchid not like", value, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidIn(List<String> values) {
            addCriterion("mchid in", values, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotIn(List<String> values) {
            addCriterion("mchid not in", values, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidBetween(String value1, String value2) {
            addCriterion("mchid between", value1, value2, "mchid");
            return (Criteria) this;
        }

        public Criteria andMchidNotBetween(String value1, String value2) {
            addCriterion("mchid not between", value1, value2, "mchid");
            return (Criteria) this;
        }

        public Criteria andDateIsNull() {
            addCriterion("date is null");
            return (Criteria) this;
        }

        public Criteria andDateIsNotNull() {
            addCriterion("date is not null");
            return (Criteria) this;
        }

        public Criteria andDateEqualTo(String value) {
            addCriterion("date =", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotEqualTo(String value) {
            addCriterion("date <>", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThan(String value) {
            addCriterion("date >", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateGreaterThanOrEqualTo(String value) {
            addCriterion("date >=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThan(String value) {
            addCriterion("date <", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLessThanOrEqualTo(String value) {
            addCriterion("date <=", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateLike(String value) {
            addCriterion("date like", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotLike(String value) {
            addCriterion("date not like", value, "date");
            return (Criteria) this;
        }

        public Criteria andDateIn(List<String> values) {
            addCriterion("date in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotIn(List<String> values) {
            addCriterion("date not in", values, "date");
            return (Criteria) this;
        }

        public Criteria andDateBetween(String value1, String value2) {
            addCriterion("date between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andDateNotBetween(String value1, String value2) {
            addCriterion("date not between", value1, value2, "date");
            return (Criteria) this;
        }

        public Criteria andWxorderIsNull() {
            addCriterion("wxorder is null");
            return (Criteria) this;
        }

        public Criteria andWxorderIsNotNull() {
            addCriterion("wxorder is not null");
            return (Criteria) this;
        }

        public Criteria andWxorderEqualTo(String value) {
            addCriterion("wxorder =", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderNotEqualTo(String value) {
            addCriterion("wxorder <>", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderGreaterThan(String value) {
            addCriterion("wxorder >", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderGreaterThanOrEqualTo(String value) {
            addCriterion("wxorder >=", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderLessThan(String value) {
            addCriterion("wxorder <", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderLessThanOrEqualTo(String value) {
            addCriterion("wxorder <=", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderLike(String value) {
            addCriterion("wxorder like", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderNotLike(String value) {
            addCriterion("wxorder not like", value, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderIn(List<String> values) {
            addCriterion("wxorder in", values, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderNotIn(List<String> values) {
            addCriterion("wxorder not in", values, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderBetween(String value1, String value2) {
            addCriterion("wxorder between", value1, value2, "wxorder");
            return (Criteria) this;
        }

        public Criteria andWxorderNotBetween(String value1, String value2) {
            addCriterion("wxorder not between", value1, value2, "wxorder");
            return (Criteria) this;
        }

        public Criteria andBzorderIsNull() {
            addCriterion("bzorder is null");
            return (Criteria) this;
        }

        public Criteria andBzorderIsNotNull() {
            addCriterion("bzorder is not null");
            return (Criteria) this;
        }

        public Criteria andBzorderEqualTo(String value) {
            addCriterion("bzorder =", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotEqualTo(String value) {
            addCriterion("bzorder <>", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderGreaterThan(String value) {
            addCriterion("bzorder >", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderGreaterThanOrEqualTo(String value) {
            addCriterion("bzorder >=", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderLessThan(String value) {
            addCriterion("bzorder <", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderLessThanOrEqualTo(String value) {
            addCriterion("bzorder <=", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderLike(String value) {
            addCriterion("bzorder like", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotLike(String value) {
            addCriterion("bzorder not like", value, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderIn(List<String> values) {
            addCriterion("bzorder in", values, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotIn(List<String> values) {
            addCriterion("bzorder not in", values, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderBetween(String value1, String value2) {
            addCriterion("bzorder between", value1, value2, "bzorder");
            return (Criteria) this;
        }

        public Criteria andBzorderNotBetween(String value1, String value2) {
            addCriterion("bzorder not between", value1, value2, "bzorder");
            return (Criteria) this;
        }

        public Criteria andTradestatusIsNull() {
            addCriterion("tradestatus is null");
            return (Criteria) this;
        }

        public Criteria andTradestatusIsNotNull() {
            addCriterion("tradestatus is not null");
            return (Criteria) this;
        }

        public Criteria andTradestatusEqualTo(String value) {
            addCriterion("tradestatus =", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusNotEqualTo(String value) {
            addCriterion("tradestatus <>", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusGreaterThan(String value) {
            addCriterion("tradestatus >", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusGreaterThanOrEqualTo(String value) {
            addCriterion("tradestatus >=", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusLessThan(String value) {
            addCriterion("tradestatus <", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusLessThanOrEqualTo(String value) {
            addCriterion("tradestatus <=", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusLike(String value) {
            addCriterion("tradestatus like", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusNotLike(String value) {
            addCriterion("tradestatus not like", value, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusIn(List<String> values) {
            addCriterion("tradestatus in", values, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusNotIn(List<String> values) {
            addCriterion("tradestatus not in", values, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusBetween(String value1, String value2) {
            addCriterion("tradestatus between", value1, value2, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTradestatusNotBetween(String value1, String value2) {
            addCriterion("tradestatus not between", value1, value2, "tradestatus");
            return (Criteria) this;
        }

        public Criteria andTimeIsNull() {
            addCriterion("time is null");
            return (Criteria) this;
        }

        public Criteria andTimeIsNotNull() {
            addCriterion("time is not null");
            return (Criteria) this;
        }

        public Criteria andTimeEqualTo(String value) {
            addCriterion("time =", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotEqualTo(String value) {
            addCriterion("time <>", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeGreaterThan(String value) {
            addCriterion("time >", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeGreaterThanOrEqualTo(String value) {
            addCriterion("time >=", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeLessThan(String value) {
            addCriterion("time <", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeLessThanOrEqualTo(String value) {
            addCriterion("time <=", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeLike(String value) {
            addCriterion("time like", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotLike(String value) {
            addCriterion("time not like", value, "time");
            return (Criteria) this;
        }

        public Criteria andTimeIn(List<String> values) {
            addCriterion("time in", values, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotIn(List<String> values) {
            addCriterion("time not in", values, "time");
            return (Criteria) this;
        }

        public Criteria andTimeBetween(String value1, String value2) {
            addCriterion("time between", value1, value2, "time");
            return (Criteria) this;
        }

        public Criteria andTimeNotBetween(String value1, String value2) {
            addCriterion("time not between", value1, value2, "time");
            return (Criteria) this;
        }

        public Criteria andGhidIsNull() {
            addCriterion("ghid is null");
            return (Criteria) this;
        }

        public Criteria andGhidIsNotNull() {
            addCriterion("ghid is not null");
            return (Criteria) this;
        }

        public Criteria andGhidEqualTo(String value) {
            addCriterion("ghid =", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidNotEqualTo(String value) {
            addCriterion("ghid <>", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidGreaterThan(String value) {
            addCriterion("ghid >", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidGreaterThanOrEqualTo(String value) {
            addCriterion("ghid >=", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidLessThan(String value) {
            addCriterion("ghid <", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidLessThanOrEqualTo(String value) {
            addCriterion("ghid <=", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidLike(String value) {
            addCriterion("ghid like", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidNotLike(String value) {
            addCriterion("ghid not like", value, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidIn(List<String> values) {
            addCriterion("ghid in", values, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidNotIn(List<String> values) {
            addCriterion("ghid not in", values, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidBetween(String value1, String value2) {
            addCriterion("ghid between", value1, value2, "ghid");
            return (Criteria) this;
        }

        public Criteria andGhidNotBetween(String value1, String value2) {
            addCriterion("ghid not between", value1, value2, "ghid");
            return (Criteria) this;
        }

        public Criteria andMernoIsNull() {
            addCriterion("merno is null");
            return (Criteria) this;
        }

        public Criteria andMernoIsNotNull() {
            addCriterion("merno is not null");
            return (Criteria) this;
        }

        public Criteria andMernoEqualTo(String value) {
            addCriterion("merno =", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoNotEqualTo(String value) {
            addCriterion("merno <>", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoGreaterThan(String value) {
            addCriterion("merno >", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoGreaterThanOrEqualTo(String value) {
            addCriterion("merno >=", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoLessThan(String value) {
            addCriterion("merno <", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoLessThanOrEqualTo(String value) {
            addCriterion("merno <=", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoLike(String value) {
            addCriterion("merno like", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoNotLike(String value) {
            addCriterion("merno not like", value, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoIn(List<String> values) {
            addCriterion("merno in", values, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoNotIn(List<String> values) {
            addCriterion("merno not in", values, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoBetween(String value1, String value2) {
            addCriterion("merno between", value1, value2, "merno");
            return (Criteria) this;
        }

        public Criteria andMernoNotBetween(String value1, String value2) {
            addCriterion("merno not between", value1, value2, "merno");
            return (Criteria) this;
        }

        public Criteria andSubmchIsNull() {
            addCriterion("submch is null");
            return (Criteria) this;
        }

        public Criteria andSubmchIsNotNull() {
            addCriterion("submch is not null");
            return (Criteria) this;
        }

        public Criteria andSubmchEqualTo(String value) {
            addCriterion("submch =", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchNotEqualTo(String value) {
            addCriterion("submch <>", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchGreaterThan(String value) {
            addCriterion("submch >", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchGreaterThanOrEqualTo(String value) {
            addCriterion("submch >=", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchLessThan(String value) {
            addCriterion("submch <", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchLessThanOrEqualTo(String value) {
            addCriterion("submch <=", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchLike(String value) {
            addCriterion("submch like", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchNotLike(String value) {
            addCriterion("submch not like", value, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchIn(List<String> values) {
            addCriterion("submch in", values, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchNotIn(List<String> values) {
            addCriterion("submch not in", values, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchBetween(String value1, String value2) {
            addCriterion("submch between", value1, value2, "submch");
            return (Criteria) this;
        }

        public Criteria andSubmchNotBetween(String value1, String value2) {
            addCriterion("submch not between", value1, value2, "submch");
            return (Criteria) this;
        }

        public Criteria andDeviceidIsNull() {
            addCriterion("deviceid is null");
            return (Criteria) this;
        }

        public Criteria andDeviceidIsNotNull() {
            addCriterion("deviceid is not null");
            return (Criteria) this;
        }

        public Criteria andDeviceidEqualTo(String value) {
            addCriterion("deviceid =", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotEqualTo(String value) {
            addCriterion("deviceid <>", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidGreaterThan(String value) {
            addCriterion("deviceid >", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidGreaterThanOrEqualTo(String value) {
            addCriterion("deviceid >=", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLessThan(String value) {
            addCriterion("deviceid <", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLessThanOrEqualTo(String value) {
            addCriterion("deviceid <=", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidLike(String value) {
            addCriterion("deviceid like", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotLike(String value) {
            addCriterion("deviceid not like", value, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidIn(List<String> values) {
            addCriterion("deviceid in", values, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotIn(List<String> values) {
            addCriterion("deviceid not in", values, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidBetween(String value1, String value2) {
            addCriterion("deviceid between", value1, value2, "deviceid");
            return (Criteria) this;
        }

        public Criteria andDeviceidNotBetween(String value1, String value2) {
            addCriterion("deviceid not between", value1, value2, "deviceid");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNull() {
            addCriterion("openid is null");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNotNull() {
            addCriterion("openid is not null");
            return (Criteria) this;
        }

        public Criteria andOpenidEqualTo(String value) {
            addCriterion("openid =", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotEqualTo(String value) {
            addCriterion("openid <>", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThan(String value) {
            addCriterion("openid >", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThanOrEqualTo(String value) {
            addCriterion("openid >=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThan(String value) {
            addCriterion("openid <", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThanOrEqualTo(String value) {
            addCriterion("openid <=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLike(String value) {
            addCriterion("openid like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotLike(String value) {
            addCriterion("openid not like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidIn(List<String> values) {
            addCriterion("openid in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotIn(List<String> values) {
            addCriterion("openid not in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidBetween(String value1, String value2) {
            addCriterion("openid between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotBetween(String value1, String value2) {
            addCriterion("openid not between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andTradetypeIsNull() {
            addCriterion("tradetype is null");
            return (Criteria) this;
        }

        public Criteria andTradetypeIsNotNull() {
            addCriterion("tradetype is not null");
            return (Criteria) this;
        }

        public Criteria andTradetypeEqualTo(String value) {
            addCriterion("tradetype =", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotEqualTo(String value) {
            addCriterion("tradetype <>", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeGreaterThan(String value) {
            addCriterion("tradetype >", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeGreaterThanOrEqualTo(String value) {
            addCriterion("tradetype >=", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeLessThan(String value) {
            addCriterion("tradetype <", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeLessThanOrEqualTo(String value) {
            addCriterion("tradetype <=", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeLike(String value) {
            addCriterion("tradetype like", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotLike(String value) {
            addCriterion("tradetype not like", value, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeIn(List<String> values) {
            addCriterion("tradetype in", values, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotIn(List<String> values) {
            addCriterion("tradetype not in", values, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeBetween(String value1, String value2) {
            addCriterion("tradetype between", value1, value2, "tradetype");
            return (Criteria) this;
        }

        public Criteria andTradetypeNotBetween(String value1, String value2) {
            addCriterion("tradetype not between", value1, value2, "tradetype");
            return (Criteria) this;
        }

        public Criteria andBankIsNull() {
            addCriterion("bank is null");
            return (Criteria) this;
        }

        public Criteria andBankIsNotNull() {
            addCriterion("bank is not null");
            return (Criteria) this;
        }

        public Criteria andBankEqualTo(String value) {
            addCriterion("bank =", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotEqualTo(String value) {
            addCriterion("bank <>", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThan(String value) {
            addCriterion("bank >", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThanOrEqualTo(String value) {
            addCriterion("bank >=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThan(String value) {
            addCriterion("bank <", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThanOrEqualTo(String value) {
            addCriterion("bank <=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLike(String value) {
            addCriterion("bank like", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotLike(String value) {
            addCriterion("bank not like", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankIn(List<String> values) {
            addCriterion("bank in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotIn(List<String> values) {
            addCriterion("bank not in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankBetween(String value1, String value2) {
            addCriterion("bank between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotBetween(String value1, String value2) {
            addCriterion("bank not between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNull() {
            addCriterion("currency is null");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNotNull() {
            addCriterion("currency is not null");
            return (Criteria) this;
        }

        public Criteria andCurrencyEqualTo(String value) {
            addCriterion("currency =", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotEqualTo(String value) {
            addCriterion("currency <>", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThan(String value) {
            addCriterion("currency >", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("currency >=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThan(String value) {
            addCriterion("currency <", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThanOrEqualTo(String value) {
            addCriterion("currency <=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLike(String value) {
            addCriterion("currency like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotLike(String value) {
            addCriterion("currency not like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyIn(List<String> values) {
            addCriterion("currency in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotIn(List<String> values) {
            addCriterion("currency not in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyBetween(String value1, String value2) {
            addCriterion("currency between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotBetween(String value1, String value2) {
            addCriterion("currency not between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIsNull() {
            addCriterion("totalmoney is null");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIsNotNull() {
            addCriterion("totalmoney is not null");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyEqualTo(BigDecimal value) {
            addCriterion("totalmoney =", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotEqualTo(BigDecimal value) {
            addCriterion("totalmoney <>", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyGreaterThan(BigDecimal value) {
            addCriterion("totalmoney >", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("totalmoney >=", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyLessThan(BigDecimal value) {
            addCriterion("totalmoney <", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("totalmoney <=", value, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyIn(List<BigDecimal> values) {
            addCriterion("totalmoney in", values, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotIn(List<BigDecimal> values) {
            addCriterion("totalmoney not in", values, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalmoney between", value1, value2, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andTotalmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalmoney not between", value1, value2, "totalmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyIsNull() {
            addCriterion("redpacketmoney is null");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyIsNotNull() {
            addCriterion("redpacketmoney is not null");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney =", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyNotEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney <>", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyGreaterThan(BigDecimal value) {
            addCriterion("redpacketmoney >", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney >=", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyLessThan(BigDecimal value) {
            addCriterion("redpacketmoney <", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketmoney <=", value, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyIn(List<BigDecimal> values) {
            addCriterion("redpacketmoney in", values, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyNotIn(List<BigDecimal> values) {
            addCriterion("redpacketmoney not in", values, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketmoney between", value1, value2, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketmoney not between", value1, value2, "redpacketmoney");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderIsNull() {
            addCriterion("wxrefundorder is null");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderIsNotNull() {
            addCriterion("wxrefundorder is not null");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderEqualTo(String value) {
            addCriterion("wxrefundorder =", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderNotEqualTo(String value) {
            addCriterion("wxrefundorder <>", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderGreaterThan(String value) {
            addCriterion("wxrefundorder >", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderGreaterThanOrEqualTo(String value) {
            addCriterion("wxrefundorder >=", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderLessThan(String value) {
            addCriterion("wxrefundorder <", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderLessThanOrEqualTo(String value) {
            addCriterion("wxrefundorder <=", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderLike(String value) {
            addCriterion("wxrefundorder like", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderNotLike(String value) {
            addCriterion("wxrefundorder not like", value, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderIn(List<String> values) {
            addCriterion("wxrefundorder in", values, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderNotIn(List<String> values) {
            addCriterion("wxrefundorder not in", values, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderBetween(String value1, String value2) {
            addCriterion("wxrefundorder between", value1, value2, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andWxrefundorderNotBetween(String value1, String value2) {
            addCriterion("wxrefundorder not between", value1, value2, "wxrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderIsNull() {
            addCriterion("bzrefundorder is null");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderIsNotNull() {
            addCriterion("bzrefundorder is not null");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderEqualTo(String value) {
            addCriterion("bzrefundorder =", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderNotEqualTo(String value) {
            addCriterion("bzrefundorder <>", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderGreaterThan(String value) {
            addCriterion("bzrefundorder >", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderGreaterThanOrEqualTo(String value) {
            addCriterion("bzrefundorder >=", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderLessThan(String value) {
            addCriterion("bzrefundorder <", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderLessThanOrEqualTo(String value) {
            addCriterion("bzrefundorder <=", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderLike(String value) {
            addCriterion("bzrefundorder like", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderNotLike(String value) {
            addCriterion("bzrefundorder not like", value, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderIn(List<String> values) {
            addCriterion("bzrefundorder in", values, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderNotIn(List<String> values) {
            addCriterion("bzrefundorder not in", values, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderBetween(String value1, String value2) {
            addCriterion("bzrefundorder between", value1, value2, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andBzrefundorderNotBetween(String value1, String value2) {
            addCriterion("bzrefundorder not between", value1, value2, "bzrefundorder");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyIsNull() {
            addCriterion("refundmoney is null");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyIsNotNull() {
            addCriterion("refundmoney is not null");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyEqualTo(BigDecimal value) {
            addCriterion("refundmoney =", value, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyNotEqualTo(BigDecimal value) {
            addCriterion("refundmoney <>", value, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyGreaterThan(BigDecimal value) {
            addCriterion("refundmoney >", value, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("refundmoney >=", value, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyLessThan(BigDecimal value) {
            addCriterion("refundmoney <", value, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("refundmoney <=", value, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyIn(List<BigDecimal> values) {
            addCriterion("refundmoney in", values, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyNotIn(List<BigDecimal> values) {
            addCriterion("refundmoney not in", values, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("refundmoney between", value1, value2, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("refundmoney not between", value1, value2, "refundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyIsNull() {
            addCriterion("redpacketrefundmoney is null");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyIsNotNull() {
            addCriterion("redpacketrefundmoney is not null");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyEqualTo(BigDecimal value) {
            addCriterion("redpacketrefundmoney =", value, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyNotEqualTo(BigDecimal value) {
            addCriterion("redpacketrefundmoney <>", value, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyGreaterThan(BigDecimal value) {
            addCriterion("redpacketrefundmoney >", value, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketrefundmoney >=", value, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyLessThan(BigDecimal value) {
            addCriterion("redpacketrefundmoney <", value, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("redpacketrefundmoney <=", value, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyIn(List<BigDecimal> values) {
            addCriterion("redpacketrefundmoney in", values, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyNotIn(List<BigDecimal> values) {
            addCriterion("redpacketrefundmoney not in", values, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketrefundmoney between", value1, value2, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRedpacketrefundmoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("redpacketrefundmoney not between", value1, value2, "redpacketrefundmoney");
            return (Criteria) this;
        }

        public Criteria andRefundtypeIsNull() {
            addCriterion("refundtype is null");
            return (Criteria) this;
        }

        public Criteria andRefundtypeIsNotNull() {
            addCriterion("refundtype is not null");
            return (Criteria) this;
        }

        public Criteria andRefundtypeEqualTo(String value) {
            addCriterion("refundtype =", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeNotEqualTo(String value) {
            addCriterion("refundtype <>", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeGreaterThan(String value) {
            addCriterion("refundtype >", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeGreaterThanOrEqualTo(String value) {
            addCriterion("refundtype >=", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeLessThan(String value) {
            addCriterion("refundtype <", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeLessThanOrEqualTo(String value) {
            addCriterion("refundtype <=", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeLike(String value) {
            addCriterion("refundtype like", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeNotLike(String value) {
            addCriterion("refundtype not like", value, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeIn(List<String> values) {
            addCriterion("refundtype in", values, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeNotIn(List<String> values) {
            addCriterion("refundtype not in", values, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeBetween(String value1, String value2) {
            addCriterion("refundtype between", value1, value2, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundtypeNotBetween(String value1, String value2) {
            addCriterion("refundtype not between", value1, value2, "refundtype");
            return (Criteria) this;
        }

        public Criteria andRefundstatusIsNull() {
            addCriterion("refundstatus is null");
            return (Criteria) this;
        }

        public Criteria andRefundstatusIsNotNull() {
            addCriterion("refundstatus is not null");
            return (Criteria) this;
        }

        public Criteria andRefundstatusEqualTo(String value) {
            addCriterion("refundstatus =", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotEqualTo(String value) {
            addCriterion("refundstatus <>", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusGreaterThan(String value) {
            addCriterion("refundstatus >", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusGreaterThanOrEqualTo(String value) {
            addCriterion("refundstatus >=", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusLessThan(String value) {
            addCriterion("refundstatus <", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusLessThanOrEqualTo(String value) {
            addCriterion("refundstatus <=", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusLike(String value) {
            addCriterion("refundstatus like", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotLike(String value) {
            addCriterion("refundstatus not like", value, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusIn(List<String> values) {
            addCriterion("refundstatus in", values, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotIn(List<String> values) {
            addCriterion("refundstatus not in", values, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusBetween(String value1, String value2) {
            addCriterion("refundstatus between", value1, value2, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andRefundstatusNotBetween(String value1, String value2) {
            addCriterion("refundstatus not between", value1, value2, "refundstatus");
            return (Criteria) this;
        }

        public Criteria andCommoditynameIsNull() {
            addCriterion("commodityname is null");
            return (Criteria) this;
        }

        public Criteria andCommoditynameIsNotNull() {
            addCriterion("commodityname is not null");
            return (Criteria) this;
        }

        public Criteria andCommoditynameEqualTo(String value) {
            addCriterion("commodityname =", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameNotEqualTo(String value) {
            addCriterion("commodityname <>", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameGreaterThan(String value) {
            addCriterion("commodityname >", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameGreaterThanOrEqualTo(String value) {
            addCriterion("commodityname >=", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameLessThan(String value) {
            addCriterion("commodityname <", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameLessThanOrEqualTo(String value) {
            addCriterion("commodityname <=", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameLike(String value) {
            addCriterion("commodityname like", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameNotLike(String value) {
            addCriterion("commodityname not like", value, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameIn(List<String> values) {
            addCriterion("commodityname in", values, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameNotIn(List<String> values) {
            addCriterion("commodityname not in", values, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameBetween(String value1, String value2) {
            addCriterion("commodityname between", value1, value2, "commodityname");
            return (Criteria) this;
        }

        public Criteria andCommoditynameNotBetween(String value1, String value2) {
            addCriterion("commodityname not between", value1, value2, "commodityname");
            return (Criteria) this;
        }

        public Criteria andDatapacketIsNull() {
            addCriterion("datapacket is null");
            return (Criteria) this;
        }

        public Criteria andDatapacketIsNotNull() {
            addCriterion("datapacket is not null");
            return (Criteria) this;
        }

        public Criteria andDatapacketEqualTo(String value) {
            addCriterion("datapacket =", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketNotEqualTo(String value) {
            addCriterion("datapacket <>", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketGreaterThan(String value) {
            addCriterion("datapacket >", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketGreaterThanOrEqualTo(String value) {
            addCriterion("datapacket >=", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketLessThan(String value) {
            addCriterion("datapacket <", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketLessThanOrEqualTo(String value) {
            addCriterion("datapacket <=", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketLike(String value) {
            addCriterion("datapacket like", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketNotLike(String value) {
            addCriterion("datapacket not like", value, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketIn(List<String> values) {
            addCriterion("datapacket in", values, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketNotIn(List<String> values) {
            addCriterion("datapacket not in", values, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketBetween(String value1, String value2) {
            addCriterion("datapacket between", value1, value2, "datapacket");
            return (Criteria) this;
        }

        public Criteria andDatapacketNotBetween(String value1, String value2) {
            addCriterion("datapacket not between", value1, value2, "datapacket");
            return (Criteria) this;
        }

        public Criteria andFeeIsNull() {
            addCriterion("fee is null");
            return (Criteria) this;
        }

        public Criteria andFeeIsNotNull() {
            addCriterion("fee is not null");
            return (Criteria) this;
        }

        public Criteria andFeeEqualTo(BigDecimal value) {
            addCriterion("fee =", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotEqualTo(BigDecimal value) {
            addCriterion("fee <>", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThan(BigDecimal value) {
            addCriterion("fee >", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fee >=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThan(BigDecimal value) {
            addCriterion("fee <", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fee <=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeIn(List<BigDecimal> values) {
            addCriterion("fee in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotIn(List<BigDecimal> values) {
            addCriterion("fee not in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee not between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andRateIsNull() {
            addCriterion("rate is null");
            return (Criteria) this;
        }

        public Criteria andRateIsNotNull() {
            addCriterion("rate is not null");
            return (Criteria) this;
        }

        public Criteria andRateEqualTo(String value) {
            addCriterion("rate =", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotEqualTo(String value) {
            addCriterion("rate <>", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThan(String value) {
            addCriterion("rate >", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThanOrEqualTo(String value) {
            addCriterion("rate >=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThan(String value) {
            addCriterion("rate <", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThanOrEqualTo(String value) {
            addCriterion("rate <=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLike(String value) {
            addCriterion("rate like", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotLike(String value) {
            addCriterion("rate not like", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateIn(List<String> values) {
            addCriterion("rate in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotIn(List<String> values) {
            addCriterion("rate not in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateBetween(String value1, String value2) {
            addCriterion("rate between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotBetween(String value1, String value2) {
            addCriterion("rate not between", value1, value2, "rate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}